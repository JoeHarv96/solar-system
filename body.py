from vector import Vector

class BodyBase:
    def __init__(self, pos, vel, mass, size, colour, name) -> None:
        self.position = pos
        self.velocity = vel
        self.mass = mass
        self.size = size
        self.colour = colour
        self.name = name
        self.G = 1

    def update_pos(self, time_step):
        self.position = (
            self.position[0] + (self.velocity[0] * time_step),
            self.position[1] + (self.velocity[1] * time_step),
            self.position[2] + (self.velocity[2] * time_step),
        )

    def update_velocity(self, second_body, time_step):
        raise NotImplementedError

    def potential_energy(self, second_body):
        distance = Vector(*self.position) - Vector(*second_body.position)
        return (-self.G * (self.mass * second_body.mass)) / distance.magnitude

    def kinetic_energy(self):
        return (self.velocity * self.velocity) * 0.5 * self.mass


class BodyNewtonian(BodyBase):
    def update_velocity(self, second_body, time_step):
        distance = Vector(*second_body.position) - Vector(*self.position)
        magnitude = distance.magnitude
        force_mag = (self.mass * second_body.mass) / (magnitude ** 2)
        force = distance.normalize() * force_mag

        acceleration = force / self.mass
        self.velocity = self.velocity + (acceleration *time_step)

class BodyLagrangian(BodyBase):
    def update_velocity(self, second_body, time_step):
        pass