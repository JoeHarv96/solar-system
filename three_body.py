from system import System
from body import BodyNewtonian
from vector import Vector


first_pos = (40, 0, 0)
first_vel = Vector(0, -7, 0)
first_mass = 10000

first = BodyNewtonian(first_pos, first_vel, first_mass, 25, "yellow", "first")

second_pos = (-40, 0, 0)
second_vel = Vector(0, 7, 0)
second_mass = 10000

second = BodyNewtonian(second_pos, second_vel, second_mass, 25, "red", "second")

third_pos = (0, 0, 0)
third_vel = Vector(0, 0, 0)
third_mass = 10000

third = BodyNewtonian(third_pos, third_vel, third_mass, 50, "orange", "third")

planet_pos = (200, 0, 0)
planet_vel = Vector(0, 10, 0)
planet_mass = 10

planet = BodyNewtonian(planet_pos, planet_vel, planet_mass, 10, "grey", "planet")


system = System([first, second, planet], 400)

system.run(1)