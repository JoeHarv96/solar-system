class Vector:
    def __init__(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z

    def __getitem__(self, index):
        data = [self.x,self.y,self.z]
        return data[index]

    def __add__(self, other):
        return Vector(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
        )

    def __sub__(self, other):
        return Vector(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
        )

    def __mul__(self, value):
        if isinstance(value, int) or isinstance(value, float):
            return Vector(
                self.x * value,
                self.y * value,
                self.z * value,
            )
        elif isinstance(value, Vector):
            return self.x * value.x + self.y * value.y + self.z * value.z

    def __truediv__(self, value):
        return Vector(
            self.x / value,
            self.y / value,
            self.z / value,
        )

    def __str__(self) -> str:
        return f"{self.x}, {self.y}, {self.z}"

    @property
    def magnitude(self):
        return ((self.x ** 2) + (self.y ** 2) + (self.z ** 2)) ** 0.5

    def normalize(self):
        return Vector(
            self.x / self.magnitude,
            self.y / self.magnitude,
            self.z / self.magnitude,
        )

