from time import sleep
import matplotlib.pyplot as plt

class System:
    def __init__(self, bodies, size) -> None:
        self.bodies = bodies
        self.size = size

        self.fig, self.ax = plt.subplots(
            1,
            1,
            subplot_kw={"projection": "3d"},
            figsize=(self.size / 50, self.size / 50),
        )
        self.fig.tight_layout()


        self.potential = []
        self.kinetic_sun = []
        self.kinetic_earth = []
        self.full_total = []

    def tick(self, time_step):
        for i, body_i in enumerate(self.bodies):
            for j, body_j in enumerate(self.bodies):
                if i == j:
                    continue
                body_i.update_velocity(body_j, time_step)


        self.kinetic_sun.append(self.bodies[0].kinetic_energy())
        self.potential.append(self.bodies[0].potential_energy(self.bodies[1]))
        self.kinetic_earth.append(self.bodies[1].kinetic_energy())
        self.full_total.append(self.bodies[0].kinetic_energy() + self.bodies[1].kinetic_energy() + self.bodies[0].potential_energy(self.bodies[1]))

        for body in self.bodies:
            body.update_pos(time_step)
            self.draw(body)

        self.clear_plots()


    def clear_plots(self):
        plt.pause(0.00001)
        self.ax.clear()

    def draw(self, body):
        self.ax.plot(
            *body.position,
            marker="o",
            markersize=body.size,
            color=body.colour
        )

        self.ax.set_xlim((-self.size / 2, self.size / 2))
        self.ax.set_ylim((-self.size / 2, self.size / 2))
        self.ax.set_zlim((-self.size / 2, self.size / 2))

    def run(self, time_step):
        while True:
            self.tick(time_step)

    def plot_lagranian_details(self):
        plt.plot(self.potential, label="potential total")
        plt.plot(self.kinetic_sun, label="kinetic sun")
        plt.plot(self.kinetic_earth, label="kinetic earth")
        plt.plot(self.full_total, label="full total")
        plt.legend(loc="upper left")

        plt.show()
