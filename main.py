from system import System
from body import BodyNewtonian
from vector import Vector

sun_pos = (0, 0, 0)
sun_vel = Vector(0, 0, 0)
sun_mass = 10000

sun = BodyNewtonian(sun_pos, sun_vel, sun_mass, 50, "yellow", "sun")

earth_pos = (100, 0, 0)
earth_vel = Vector(0, 8, 0)
earth_mass = 20

earth = BodyNewtonian(earth_pos, earth_vel, earth_mass, 10, "green", "earth")

moon_pos = (160, 0, 0)
moon_vel = Vector(0, 10, 0)
moon_mass = 20
moon = BodyNewtonian(moon_pos, moon_vel, moon_mass, 5, "grey", "moon")


planet_terror_pos = (-150, 50, 0)
planet_terror_vol = Vector(0, 5, 5)
planet_terror_mass = 20
terror = BodyNewtonian(planet_terror_pos, planet_terror_vol, planet_terror_mass, 10, "red", "terror")


system = System([sun, earth],  400)

system.run(0.1)
